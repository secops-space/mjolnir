// Copyright © 2019 Bren Briggs <code@fraq.io>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/secops-space/mjolnir/mjolnir"
)

const Version = ""

var (
	cfgFile  string
	server   string
	channels []string
	nick     string
	ssl      bool
	nickserv string
	operUser string
	operPass string
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Version: Version,
	Use:     "mjolnir",
	Short:   "Manage unwanted users in your IRCd",
	Run: func(cmd *cobra.Command, args []string) {
		config := mjolnir.Config{
			NickservPass: viper.GetString("nickserv"),
			OperUser:     viper.GetString("operUser"),
			OperPass:     viper.GetString("operPass"),
		}
		mjolnir.Run(viper.GetString("server"), viper.GetString("nick"), viper.GetStringSlice("channels"), viper.GetBool("ssl"), config)
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.mjolnir.yaml)")
	rootCmd.PersistentFlags().StringVarP(&server, "server", "s", server, "target server")
	rootCmd.PersistentFlags().StringVarP(&nickserv, "nickserv", "", nickserv, "nickserv password")
	rootCmd.PersistentFlags().StringVarP(&operUser, "operUser", "", operUser, "oper user")
	rootCmd.PersistentFlags().StringVarP(&operPass, "operPass", "", operPass, "oper password")
	rootCmd.PersistentFlags().StringSliceVarP(&channels, "channels", "c", channels, "channels to join")
	rootCmd.PersistentFlags().StringVarP(&nick, "nick", "n", nick, "nickname")
	rootCmd.PersistentFlags().BoolVarP(&ssl, "ssl", "", ssl, "enable ssl")

	viper.BindPFlag("server", rootCmd.PersistentFlags().Lookup("server"))
	viper.BindPFlag("nickserv", rootCmd.PersistentFlags().Lookup("nickserv"))
	viper.BindPFlag("operUser", rootCmd.PersistentFlags().Lookup("operUser"))
	viper.BindPFlag("operPass", rootCmd.PersistentFlags().Lookup("operPass"))
	viper.BindPFlag("channels", rootCmd.PersistentFlags().Lookup("channels"))
	viper.BindPFlag("nick", rootCmd.PersistentFlags().Lookup("nick"))
	viper.BindPFlag("ssl", rootCmd.PersistentFlags().Lookup("ssl"))

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		viper.AddConfigPath(".")
		viper.SetConfigName("mjolnir")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
