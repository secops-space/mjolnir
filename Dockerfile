FROM golang:1.12-alpine as builder

RUN adduser -D -g 'mjolnir' mjolnir
WORKDIR /app
COPY . .
RUN apk add --no-cache git
RUN ./build.sh
RUN touch .bolt.db

FROM scratch
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /app/opt/mjolnir /opt/mjolnir
COPY --from=builder --chown=mjolnir:mjolnir /app/.bolt.db .
USER mjolnir
ENTRYPOINT ["/opt/mjolnir"]
