package mjolnir

var (
	GitVersion string = "0.1.0" // Dockerhub appears to use shallow clones which drop tag info. Set this as a default."
	GitCommit  string
	GitBranch  string
)
