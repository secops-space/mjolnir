package mjolnir

import (
	"fmt"
	"github.com/whyrusleeping/hellabot"
)

var InfoTrigger = hbot.Trigger{
	func(irc *hbot.Bot, m *hbot.Message) bool {
		return m.Command == "PRIVMSG" && m.Content == "!info"
	},
	func(irc *hbot.Bot, m *hbot.Message) bool {
		resp := fmt.Sprintf("Mjolnir version %s (%s/%s)", GitVersion, GitBranch, GitCommit)
		irc.Reply(m, resp)
		return true
	},
}
