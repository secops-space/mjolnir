package mjolnir

import (
	"fmt"
	"github.com/whyrusleeping/hellabot"
	"gopkg.in/sorcix/irc.v1"
	"time"
)

var OperLogin = hbot.Trigger{
	func(bot *hbot.Bot, m *hbot.Message) bool {
		return m.Command == irc.RPL_MYINFO // message type 004
	},
	func(bot *hbot.Bot, m *hbot.Message) bool {
		ns := b.NickservLogin()
		bot.Msg("NickServ", ns)
		time.Sleep(5 * time.Second)
		op := b.OperLogin()
		//bot.Msg("fraq", op)
		bot.Send(op)
		return true
	},
}

func (b Bot) OperLogin() string {
	//login := fmt.Sprintf("%+v", b.Config)
	login := fmt.Sprintf("OPER %s %s", b.Config.OperUser, b.Config.OperPass)
	return login
}

func (b Bot) NickservLogin() string {
	login := fmt.Sprintf("IDENTIFY %s", b.Config.NickservPass)
	return login
}
