package mjolnir

import (
	"crypto/md5"
	"encoding/binary"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/whyrusleeping/hellabot"
	bolt "go.etcd.io/bbolt"
	log "gopkg.in/inconshreveable/log15.v2"
)

var MessageTrackTrigger = hbot.Trigger{
	func(irc *hbot.Bot, m *hbot.Message) bool {
		return m.Command == "PRIVMSG"
	},
	func(irc *hbot.Bot, m *hbot.Message) bool {
		var tail []string
		var count int
		key := normalizeAndHash(m.Content)
		messageTrackLogger := log.New("key", key, "text", m.Content)
		val := parseTrackingKey(b.ReadKey(key, "repeats"))
		messageTrackLogger.Debug("fetched messageTrack key", "tail", val)

		// tail is in the format count:nick:nick:...
		if len(val) > 2 {
			count, _ = strconv.Atoi(val[1])
			count += 1
			tail = append(tail, strconv.Itoa(count))
			tail = append(tail, val[2:]...)
			tail = append(tail, m.From)
			tail = uniqueStrSlice(tail)

		} else {
			count = 1
			tail = append(tail, strconv.Itoa(count))
			tail = append(tail, m.From)
		}

		err := b.DB.Update(func(tx *bolt.Tx) error {
			b := tx.Bucket([]byte("repeats"))
			v := formatTrackingKey(timestamp(), tail)
			messageTrackLogger.Debug("writing messageTrack key", "tail", parseTrackingKey(string(v))[1:])
			return b.Put([]byte(key), v)
		})

		if err != nil {
			log.Error(err.Error())
		}

		return false // Keep processing triggers
	},
}

var RepeatMessageTrigger = hbot.Trigger{
	func(irc *hbot.Bot, m *hbot.Message) bool {
		return m.Command == "PRIVMSG"
	},
	func(irc *hbot.Bot, m *hbot.Message) bool {
		var nicks []string
		var count = 0
		key := normalizeAndHash(m.Content)
		repeatMessageLogger := log.New("key", key, "text", m.Content)
		val := parseTrackingKey(b.ReadKey(key, "repeats"))

		// Pull old nicks out, tack our new one on the end
		// We don't care about uniqueness
		if len(val) > 2 {
			nicks = val[2:]
			count, _ = strconv.Atoi(val[1])
			repeatMessageLogger.Debug("", "count", val[1])
		}

		if count > 2 {
			for _, v := range nicks {
				irc.Send(fmt.Sprintf("KILL %s repeat spam (Automated response by Mjolnir)", v))
			}
		}

		return false // Keep processing triggers
	},
}

func timestamp() []byte {
	i := time.Now().Unix()
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(i))
	return b

}

func normalizeAndHash(m string) string {
	m = strings.ToLower(m)
	m = strings.Join(strings.Fields(m), "")
	m = fmt.Sprintf("%x", md5.Sum([]byte(m)))
	log.Debug("normalizeAndHash", "hash", m)
	return m
}

func parseTrackingKey(k string) []string {
	return strings.Split(k, ":")
}

func formatTrackingKey(ts []byte, tail []string) []byte {
	var key []byte
	tailBytes := []byte(strings.Join(tail, ":"))
	key = append(key, ts...)
	key = append(key, []byte(":")...)
	key = append(key, tailBytes...)
	return key
}
